import React from "react";
import Home from "./components/Home";
import { createGlobalStyle } from "styled-components";
import { backgrounds } from "@uprise/colors";
// import { Card } from '@uprise/card'

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${backgrounds.fadedPurple};
    margin:0;
    padding:0;
    box-sizing:border-box;
    font-family: 'Roboto', sans-serif;
  }
`;
export default function App() {
  return (
    <>
      <GlobalStyle />
      <Home />
    </>
  );
}
