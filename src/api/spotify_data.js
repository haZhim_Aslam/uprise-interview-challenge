import { SpotifyGraphQLClient } from "spotify-graphql";
import { useState } from "react";
function getData() {
  const [data, setData] = useState([]);
  const [login, setLogin] = useState(false);
  function fetchData(configuration) {
    SpotifyGraphQLClient(configuration)
      .query(
        `
      {
        user(id: "Chilledcow") {
           display_name
       images{
      width
      height
      url
    }
    href
    uri
      playlists {
            name
            images{
              width
              url
              
            }
            tracks {
              track {
                id
                name
                artists {
                  id
                  name
                }
              }
            }
          }
        }
      }
`
      )
      .then(result => {
        setData(result);
        setLogin(true);
      })
      .catch(error => {
        console.log(error);

        return error;
      });
  }

  return [data, login, fetchData];
}

export default getData;
