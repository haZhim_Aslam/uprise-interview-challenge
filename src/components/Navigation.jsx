import React, { useEffect } from "react";
import { Card } from "@uprise/card";
import { backgrounds, primary, extended } from "@uprise/colors";
import Featured from "./Featured";
import Styled from "styled-components";
const NavWrapper = Styled.div`
display:flex;
padding: 1.5em;
`;
const NavItems = Styled.div`
font-size:1rem;
color:${extended.charcoal.three};
margin-right:2em;
cursor:pointer;
&:hover{
  color:${primary.purple};
}
`;
const DropDown = Styled.div`
  position: relative;
  display: inline-block;
  &:hover{
    .dropdown-content{
      display:block;
    }
  }
`;
const DropDownContent = Styled.div`
display: none;
  position: absolute;
  /* transform:translateY(25px); */
  background-color: ${backgrounds.white};
  width:200px;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 100;
  
`;
const DropDownItem = Styled.div`
padding:10px 15px;
font-size:1rem;
color:${extended.charcoal.three};
&:hover{
  background:${primary.purple};
  color:${backgrounds.white};
 }
`;
const Navigation = props => {
  const { playlists } = props.data.data.user;
  const [artistNames, setArtists] = React.useState([]);
  const artist_names = [];

  useEffect(() => {
    playlists.map(playlist => {
      return playlist.tracks.map(track => {
        return track.track.artists.map(artist => {
          return artist_names.push(artist.name);
        });
      });
    });
    setArtists(artist_names);
  }, [props, playlists]);
  useEffect(() => {
    console.log(artist_names);
  }, [artist_names]);

  return (
    <>
      <Card
        backgroundColor={backgrounds.white}
        shadow={true}
        style={{ overflow: "unset" }}
      >
        <NavWrapper>
          <NavItems onClick={() => props.handle(1)}>Overview</NavItems>
          <NavItems onClick={() => props.handle(2)}>Playlist</NavItems>
          <NavItems>
            <DropDown>
              <div> Featured </div>
              <DropDownContent className="dropdown-content">
                {artistNames.slice(0, 10).map((item, index) => {
                  return (
                    <>
                      <DropDownItem key={index}>{item}</DropDownItem>
                    </>
                  );
                })}
              </DropDownContent>
            </DropDown>
          </NavItems>
        </NavWrapper>
      </Card>
    </>
  );
};
export default Navigation;
