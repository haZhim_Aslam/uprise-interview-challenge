import React, { useEffect } from "react";
import { Card } from "@uprise/card";
import { backgrounds, extended } from "@uprise/colors";
// import { ProgressiveImage } from "@uprise/image";
import { H1 } from "@uprise/headings";
import Styled from "styled-components";
import { Button } from "@uprise/button";
const img1 = "https://i.scdn.co/image/ab67706c0000da84163aeea48afe86ed0c55bfcd";
const ImageContainer = Styled.div`
img{
  border-radius:10px;
  margin-right:2em;
}
`;

const Wrapper = Styled.div`
display:flex;

`;

const InfoContainer = Styled.div`
display:flex;
flex-direction:column;

`;
const InfoSecondary = Styled.p`
font-size:1rem;
color:${extended.charcoal.three};
`;

const Overview = props => {
  const { display_name, images, href } = props.data.data.user;
  useEffect(() => {
    console.log(display_name, images, href);
  }, []);

  return (
    <>
      <Card
        padding="30px"
        shadow={true}
        backgroundColor={backgrounds.white}
        height={"300px"}
      >
        <Wrapper>
          <ImageContainer>
            <img src={images[0].url} alt="baner" />
          </ImageContainer>
          <InfoContainer>
            <H1 style={{ margin: 0, fontFamily: "Roboto, sans-serif" }}>
              {display_name}
            </H1>
            <InfoSecondary>Followers (122,1221)</InfoSecondary>
            <div>
              <a href={href}>
                <Button variant="primary" title="follow" size="medium" />
              </a>
            </div>
          </InfoContainer>
        </Wrapper>
      </Card>
    </>
  );
};
export default Overview;
