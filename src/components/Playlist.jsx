import React from "react";
import { Card } from "@uprise/card";
import "./styles/flickity.css";
import { backgrounds, extended } from "@uprise/colors";
import Styled from "styled-components";
import Flickity from "react-flickity-component";

const Wrapper = Styled.div`
margin:0 30%;
`;
const PlaylistInfo = Styled.div`
font-size:30px;
font-weight:blod;
color:${extended.charcoal.one};
text-align:center;
`;
const flickityOptions = {
  pageDots: false
};

const Playlist = props => {
  const { playlists } = props.data.data.user;
  return (
    <>
      <Card padding="30px" shadow={true} backgroundColor={backgrounds.white}>
        <Wrapper>
          <Flickity
            options={flickityOptions}
            reloadOnUpdate={true} // default false
            static={true}
          >
            {playlists.map((item, index) => {
              return (
                <>
                  <div style={{}} key={index}>
                    {" "}
                    <img
                      src={item.images[0].url}
                      alt="cat"
                      style={{
                        height: "500px",
                        borderRadius: "10px"
                      }}
                    />
                    <PlaylistInfo> {item.name}</PlaylistInfo>
                  </div>
                </>
              );
            })}
          </Flickity>
        </Wrapper>
      </Card>
    </>
  );
};
export default Playlist;
