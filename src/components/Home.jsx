import React, { useEffect } from "react";
import Navigation from "./Navigation";
import Styled from "styled-components";
import Overview from "./Overview";
import Playlist from "./Playlist";

// import config from "../config.json";
import SpotifyLogin from "react-spotify-login";
import { clientId, redirectUri } from "./settings";
import img1 from "../assets/images/logotype.png";
import getData from "../api/spotify_data";
import { REACT_APP_CLIENT_ID, REACT_APP_CLIENT_SECRET } from "../../config.env";
const MainWrapper = Styled.div`
padding:8% 13%;
`;
const MainSection = Styled.div`
margin-top:2em;
`;
const Container = Styled.div`
display:flex;
margin:25% auto;
justify-content:center;
flex-direction:column;
align-items:center;
`;
const Home = () => {
  // const [login, setLogin] = React.useState(false);

  const [data, login, fetchData] = getData();
  const [nav, setNav] = React.useState(1);
  useEffect(() => {
    console.log(data);
  }, [data]);
  const onSuccess = response => {
    let newConfig = {
      clientId: REACT_APP_CLIENT_ID,
      clientSecret: REACT_APP_CLIENT_SECRET,
      redirectUri: "http://localhost:3000",
      accessToken: response.access_token
    };

    fetchData(newConfig);
  };
  const onFailure = response => console.error(response);

  const handleCase = data => {
    setNav(data);
  };
  return (
    <>
      {login === false ? (
        <Container>
          <img
            src={img1}
            alt="icon"
            style={{ height: "100px", width: "100px" }}
          />
          <SpotifyLogin
            clientId={clientId}
            redirectUri={redirectUri}
            onSuccess={onSuccess}
            onFailure={onFailure}
            className="login-btn"
          >
            Login with spotify
          </SpotifyLogin>
        </Container>
      ) : (
        <MainWrapper>
          <Navigation data={data} handle={handleCase} />

          <MainSection>
            {nav === 1 ? <Overview data={data} /> : <Playlist data={data} />}

            {/* {
              switch(case){
              case 1:{
                return(
                  <Overview data={data} />
                )
              }
              case 2:{
                return(
                  <Playlist data={data} />
                )
              }
              default:{
                return (
                  <Overview data={data} />
                )
              }
            }
            } */}
          </MainSection>
        </MainWrapper>
      )}
    </>
  );
};
export default Home;
